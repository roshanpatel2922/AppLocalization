//
//  ViewController.swift
//  AppLocalization
//
//  Created by Roshan Patel on 15/02/23.
//

import UIKit

extension String {
    var Local: String {
        get {
            return NSLocalizedString(self, comment: "")
        }
    }
    
    var LocalHindi: String {
        get {
            let x = Bundle.main
            let path = x.path(forResource: "hi", ofType: "lproj")!
            let bundle = Bundle(path: path)!
            
            return NSLocalizedString(self,bundle: bundle, comment: "")
            
        }
    }
    var LocalEnglish: String {
        get {
            let x = Bundle.main
            let path = x.path(forResource: "en", ofType: "lproj")!
            let bundle = Bundle(path: path)!
            
            return NSLocalizedString(self,bundle: bundle, comment: "")
        }
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var HindiLbl: UILabel!
    
    @IBOutlet weak var EngLbl: UILabel!
    
    @IBOutlet weak var MyLbl3: UILabel!
    
    @IBOutlet weak var MyLbl2: UILabel!
    
    @IBOutlet weak var MyLbl1: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
//        MyLbl1.text = NSLocalizedString("L1", value: "Default - 1",comment:"")
//        MyLbl2.text = NSLocalizedString("L2", value: "Default - 2",comment:"")
//        MyLbl3.text = NSLocalizedString("L3", comment:"")
    }
    
    @IBAction func OnClickENG(_ sender: UIButton) {
        MyLbl1.text = "L1".LocalEnglish
        MyLbl2.text = "L2".LocalEnglish
        MyLbl3.text = "L3".LocalEnglish
    }
    
    
    @IBAction func OnClickHindi(_ sender: UIButton) {
        MyLbl1.text = "L1".LocalHindi
        MyLbl2.text = "L2".LocalHindi
        MyLbl3.text = "L3".LocalHindi
    }
    
    
}

